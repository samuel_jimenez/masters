\select@language {english}
\contentsline {chapter}{Abstract}{i}{section*.2}
\contentsline {chapter}{Preface}{ii}{section*.5}
\contentsline {chapter}{Contents}{iii}{section*.7}
\contentsline {chapter}{List of Figures}{v}{section*.9}
\contentsline {chapter}{List of Tables}{vi}{section*.11}
\contentsline {chapter}{\numberline {1}Introduction}{1}{section*.12}
\contentsline {section}{\numberline {1.1}Problem Description}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Objectives and scope delimitation}{2}{section.1.2}
\contentsline {section}{\numberline {1.3}Outline}{3}{section.1.3}
\contentsline {chapter}{\numberline {2}Background and Related Work}{4}{section*.13}
\contentsline {section}{\numberline {2.1}Augmented Reality}{4}{section.2.1}
\contentsline {section}{\numberline {2.2}Leap Motion Controller}{5}{section.2.2}
\contentsline {section}{\numberline {2.3}Gesture recognition}{7}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Gestures classification}{7}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Gestures acceptance and intuitiveness}{8}{subsection.2.3.2}
\contentsline {section}{\numberline {2.4}Pinch Gesture}{9}{section.2.4}
\contentsline {section}{\numberline {2.5}Related Work}{9}{section.2.5}
\contentsline {subsection}{\numberline {2.5.1}AR interaction}{9}{subsection.2.5.1}
\contentsline {subsection}{\numberline {2.5.2}Leap Motion Approach}{10}{subsection.2.5.2}
\contentsline {subsection}{\numberline {2.5.3}AR Gesture-based Interaction}{11}{subsection.2.5.3}
\contentsline {chapter}{\numberline {3}Design and Methods}{15}{section*.22}
\contentsline {section}{\numberline {3.1}Guidelines}{15}{section.3.1}
\contentsline {section}{\numberline {3.2}Choice of frameworks}{16}{section.3.2}
\contentsline {section}{\numberline {3.3}AR Workflow}{17}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Feature tracking (Image target)}{18}{subsection.3.3.1}
\contentsline {section}{\numberline {3.4}Hand-motion tracking Workflow}{19}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}Interaction Box}{20}{subsection.3.4.1}
\contentsline {section}{\numberline {3.5}Pinch Gesture}{20}{section.3.5}
\contentsline {section}{\numberline {3.6}AR - Hand-motion tracking integration}{22}{section.3.6}
\contentsline {subsubsection}{Coordinate Systems Matching}{23}{section*.32}
\contentsline {subsubsection}{General Architecture}{24}{section*.34}
\contentsline {subsubsection}{AR-Leap Motion Tracking and Rendering}{25}{section*.36}
\contentsline {section}{\numberline {3.7}Proof of Concept for Mobile Devices}{26}{section.3.7}
\contentsline {chapter}{\numberline {4}Implementation and Evaluation}{28}{section*.41}
\contentsline {section}{\numberline {4.1}Development Tools}{28}{section.4.1}
\contentsline {section}{\numberline {4.2}AR-Leap Motion Scene}{28}{section.4.2}
\contentsline {section}{\numberline {4.3}Experimental Set-up}{30}{section.4.3}
\contentsline {section}{\numberline {4.4}Evaluation}{30}{section.4.4}
\contentsline {subsection}{\numberline {4.4.1}Test No.1}{30}{subsection.4.4.1}
\contentsline {subsubsection}{Participants}{31}{section*.45}
\contentsline {subsubsection}{Procedure}{31}{section*.47}
\contentsline {subsubsection}{Participant instructions}{32}{section*.48}
\contentsline {subsubsection}{Data}{32}{section*.50}
\contentsline {subsubsection}{System Usability Scale}{32}{section*.53}
\contentsline {subsubsection}{Questionnaire and observation}{34}{section*.54}
\contentsline {subsection}{\numberline {4.4.2}Test No. 2}{34}{subsection.4.4.2}
\contentsline {subsubsection}{Participants}{35}{section*.56}
\contentsline {subsubsection}{Data}{35}{section*.58}
\contentsline {chapter}{\numberline {5}Results and Discussion}{36}{section*.60}
\contentsline {section}{\numberline {5.1}Test No. 1}{36}{section.5.1}
\contentsline {section}{\numberline {5.2}Test No. 2}{37}{section.5.2}
\contentsline {section}{\numberline {5.3}SUS Assessment}{39}{section.5.3}
\contentsline {section}{\numberline {5.4}Questionnaire and Observations}{40}{section.5.4}
\contentsline {section}{\numberline {5.5}Discussion}{43}{section.5.5}
\contentsline {subsection}{\numberline {5.5.1}Limitations}{44}{subsection.5.5.1}
\contentsline {chapter}{\numberline {6}Conclusion and Future Work}{45}{section*.70}
\contentsline {section}{\numberline {6.1}Contributions}{45}{section.6.1}
\contentsline {section}{\numberline {6.2}Future work}{46}{section.6.2}
\contentsline {chapter}{Bibliography}{47}{section*.72}
